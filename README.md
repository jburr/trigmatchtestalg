Trigger Matching Test Alg
=========================

Provides an algorithm to test trigger matching.

There is an example run script

```bash
    python3 -m TrigMatchTestAlg.run /path/to/input -t list of triggers
```