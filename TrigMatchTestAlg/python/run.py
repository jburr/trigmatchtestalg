"""Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

The main run script"""


import argparse
import functools
from typing import List


# Run argparse *before* importing any ATLAS code to avoid slow imports on --help
parser = argparse.ArgumentParser()
parser.add_argument(
    "input_files", type=str, nargs="*", help="The names of the input files"
)
parser.add_argument(
    "-i",
    "--input",
    default=[],
    type=functools.partial(str.split, sep=","),
    help="Names of further input files separated by a comma",
)
parser.add_argument(
    "-t",
    "--triggers",
    type=str,
    nargs="*",
    help="The triggers to test"
)
parser.add_argument(
    "-l",
    "--log-level",
    type=str.upper,
    default="INFO",
    choices=["VERBOSE", "DEBUG", "INFO", "WARNING", "ERROR"],
    help="Global output messaging level",
)
parser.add_argument(
    "-n",
    "--n-events",
    type=int,
    default=-1,
    help="The number of input events to run over. -1 runs over all events",
)
args = parser.parse_args()

from AthenaCommon import Constants
from AthenaConfiguration.AthConfigFlags import AthConfigFlags
from AthenaConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaRootComps.xAODEventSelectorConfig import xAODReadCfg
from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg


ConfigFlags.Input.Files = args.input_files + args.input
ConfigFlags.Exec.MaxEvents = args.n_events
ConfigFlags.Exec.OutputLevel = getattr(Constants, args.log_level)
ConfigFlags.lock()

# Set up the CA
acc = MainServicesCfg(ConfigFlags)
acc.merge(xAODReadCfg(ConfigFlags))
# Stop it printing so much
acc.getService("AthenaEventLoopMgr").EventPrintoutInterval = 1000

# Get the TDT
tdt = acc.getPrimaryAndMerge(TrigDecisionToolCfg(ConfigFlags))

# Get the matching tool. Note that this won't deal with the composite matching for now
if ConfigFlags.Trigger.EDMVersion == 3:
    match_tool = CompFactory.Trig.R3MatchingTool(TrigDecisionTool = tdt)
else:
    match_tool = CompFactory.Trig.MatchingTool(TrigDecisionTool = tdt)

acc.addEventAlgo(
    CompFactory.TrigMatchTestAlg(
        Chains=args.triggers,
        TrigDecisionTool = tdt,
        MatchingTool = match_tool
    )
)
acc.run()