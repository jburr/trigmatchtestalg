#include "TrigMatchTestAlg.h"
#include "AsgDataHandles/ReadHandle.h"
#include "TrigCompositeUtils/ChainNameParser.h"
#include "TrigCompositeUtils/KFromNItr.h"
#include "TrigCompositeUtils/ProductItr.h"
#include "xAODBase/ObjectType.h"

namespace {
    std::ostream &operator<<(std::ostream &os, const xAOD::IParticle *part) {
        return os << xAODType::ObjectType(part->type()) << " pt=" << part->pt()
                  << " MeV, eta=" << part->eta() << ", phi=" << part->phi();
    }

    template <typename T> std::ostream &operator<<(std::ostream &os, const std::vector<T> &v) {
        os << "[";
        if (v.size() > 0) {
            for (auto itr = v.begin(); itr != v.end() - 1; ++itr)
                os << *itr << ", ";
            os << v.back();
        }
        return os << "]";
    }

} // namespace

TrigMatchTestAlg::TrigMatchTestAlg(const std::string &name, ISvcLocator *pSvcLocator)
        : EL::AnaAlgorithm(name, pSvcLocator) {
    declareProperty(
            "InputElectrons", m_readHandles["e"] = "Electrons", "The input electron container");
    declareProperty(
            "ElectronSelection", m_selectionNames["e"], "The selection on the input electrons");
    declareProperty("InputMuons", m_readHandles["mu"] = "Muons", "The input muon container");
    declareProperty("MuonSelection", m_selectionNames["mu"], "The selection on the input muons");
    declareProperty("InputPhotons", m_readHandles["g"] = "Photons", "The input photon container");
    declareProperty("PhotonSelection", m_selectionNames["g"], "The selection on the input photons");
    declareProperty("InputTaus", m_readHandles["tau"] = "TauJets", "The input tau jet container");
    declareProperty("TauSelection", m_selectionNames["tau"], "The selection on the input taus");
    declareProperty("TrigDecisionTool", m_tdt, "The trigger decision tool");
}

StatusCode TrigMatchTestAlg::initialize() {
    ANA_CHECK(m_tdt.retrieve());
    ANA_CHECK(m_matchTool.retrieve());
    for (auto itr = m_readHandles.begin(); itr != m_readHandles.end();) {
        ANA_CHECK(itr->second.initialize(SG::AllowEmpty));
        if (itr->second.empty())
            itr = m_readHandles.erase(itr);
        else
            ++itr;
    }
    for (const auto &p : m_selectionNames) {
        if (p.second.empty())
            m_selections[p.first] = std::nullopt;
        else
            m_selections[p.first] = p.second;
    }
    for (const std::string &chain : m_chains) {
        auto &conf = m_trigConf[chain];
        for (const ChainNameParser::LegInfo &info : ChainNameParser::HLTChainInfo(chain))
            if (m_readHandles.find(info.signature) != m_readHandles.end())
                conf[info.signature] += info.multiplicity;
        m_nPass[chain] = 0;
        m_nCombos[chain] = 0;
        m_nMatched[chain] = 0;
    }
    return StatusCode::SUCCESS;
}

StatusCode TrigMatchTestAlg::execute() {
    using namespace TrigCompositeUtils;
    // Retrieve input containers
    std::map<std::string, std::vector<const xAOD::IParticle *>> allContainers;
    for (const auto &p : m_readHandles) {
        SG::ReadHandle<xAOD::IParticleContainer> handle = SG::makeHandle(p.second);
        if (!handle.isValid()) {
            ANA_MSG_ERROR("Failed to retrieve " << p.second.key());
            return StatusCode::FAILURE;
        }
        auto &selected = allContainers[p.first];
        const auto &selection = m_selections.at(p.first);
        for (const xAOD::IParticle *part : *handle)
            if (!selection.has_value() || selection.value()(*part))
                selected.push_back(part);
    }
    ++m_nEvents;
    for (const std::string &chain : m_chains) {
        if (!m_tdt->isPassed(chain))
            continue;
        ++m_nPass[chain];
        // Now build the combinations
        std::vector<KFromNItr> begins;
        std::vector<std::vector<const xAOD::IParticle *>> containers;
        for (const auto &p : m_trigConf[chain]) {
            begins.emplace_back(p.second, allContainers.at(p.first).size());
            containers.emplace_back(allContainers.at(p.first));
        }
        ProductItr<KFromNItr> itr(begins, std::vector<KFromNItr>(begins.size()));
        ProductItr<KFromNItr> end;
        if (msgLevel(MSG::DEBUG))
            ANA_MSG_DEBUG(
                    "Test chain " << chain << " against " << std::distance(itr, end)
                                  << " combinations");
        for (; itr != end; ++itr) {
            ++m_nCombos[chain];
            std::vector<const xAOD::IParticle *> combination;
            for (std::size_t contIdx = 0; contIdx < containers.size(); ++contIdx)
                for (std::size_t objIdx : *(itr->at(contIdx)))
                    combination.push_back(containers.at(contIdx).at(objIdx));
            bool matched = m_matchTool->match(combination, chain);
            if (matched)
                ++m_nMatched[chain];
            if (msgLevel(MSG::VERBOSE))
                ANA_MSG_DEBUG(
                        "Combination " << combination << " is matched? " << std::boolalpha
                                       << matched);
        }
    }
    return StatusCode::SUCCESS;
}

StatusCode TrigMatchTestAlg::finalize() {
    ATH_MSG_INFO("Ran over " << m_nEvents << " events");
    for (const std::string &chain : m_chains)
        ATH_MSG_INFO(
                "Chain " << chain << " passed " << m_nPass[chain] << "/" << m_nEvents
                         << " events with " << m_nMatched[chain] << "/" << m_nCombos[chain]
                         << " combinations matched");
    return StatusCode::SUCCESS;
}

DECLARE_COMPONENT(TrigMatchTestAlg)