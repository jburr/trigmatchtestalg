#pragma once

#include "AnaAlgorithm/AnaAlgorithm.h"
#include "AsgDataHandles/ReadHandleKey.h"
#include "AsgTools/PropertyWrapper.h"
#include "AsgTools/ToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include "xAODBase/IParticleContainer.h"

#include <map>
#include <optional>
#include <string>
#include <vector>

class TrigMatchTestAlg : public EL::AnaAlgorithm {
public:
    TrigMatchTestAlg(const std::string &name, ISvcLocator *pSvcLocator);
    virtual ~TrigMatchTestAlg() override = default;

    virtual StatusCode initialize() override;
    virtual StatusCode execute() override;
    virtual StatusCode finalize() override;

private:
    Gaudi::Property<std::vector<std::string>> m_chains{this, "Chains", {}, "The chains to test"};
    std::map<std::string, SG::ReadHandleKey<xAOD::IParticleContainer>> m_readHandles;
    std::map<std::string, std::string> m_selectionNames;
    ToolHandle<Trig::IMatchingTool> m_matchTool{this, "MatchingTool", "", "The matching tool"};
    ToolHandle<Trig::TrigDecisionTool> m_tdt;
    std::map<std::string, std::map<std::string, std::size_t>> m_trigConf;
    std::map<std::string, std::optional<SG::AuxElement::ConstAccessor<char>>> m_selections;
    std::size_t m_nEvents{0};
    std::map<std::string, std::size_t> m_nPass;
    std::map<std::string, std::size_t> m_nCombos;
    std::map<std::string, std::size_t> m_nMatched;
}; //>  end class TrigMatchTestAlg